﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serijalizacija {
  public class CryptoCurrency {
    public CryptoCurrency(string name, string shortName, decimal value) {
      Name = name;
      ShortName = shortName;
      Value = value;
    }

        public override string ToString()
        {
            return Name;
        }

        public string Ispis { get
            {
                return Value + " " + ShortName;
            } 
        }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public decimal Value { get; set; }
  }
}

