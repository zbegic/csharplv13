﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace Serijalizacija {
  public class Wallet {
    private List<CryptoCurrency> currencies;

    public Wallet() {
      currencies = new List<CryptoCurrency>();
      Deserialize();
    }

    public void AddCurrency(CryptoCurrency currency) {
            currencies.Add(currency);
            Serialize();
    }

    public IEnumerable<CryptoCurrency> GetCurrencies() {
      return
        from c in currencies
        orderby c.Name
        select c;
    }

        public void Serialize()
        {
            string currenciesInJson = JsonSerializer.Serialize(currencies);
            using (StreamWriter valuteUnos = new(@"G:\ZBegicC#\Valute.txt"))
            {
                valuteUnos.WriteLine(currenciesInJson);
            }
            MessageBox.Show(currenciesInJson);
        }

        public void Deserialize()   
        {
            string json = File.ReadAllText((@"G:\ZBegicC#\Valute.txt"));
            currencies = JsonSerializer.Deserialize<List<CryptoCurrency>>(json);
        }
    }
}
