namespace Serijalizacija {
  public partial class FormWallet : Form {
    public Wallet wallet;

    public FormWallet() {
      InitializeComponent();
      wallet = new Wallet();
      cbCurrencies.Items.AddRange(wallet.GetCurrencies().ToArray());
    }

    private void btnAddCurrency_Click(object sender, EventArgs e) {
      FormCurrencyAdd formCurrencyAdd = new FormCurrencyAdd();
      if (formCurrencyAdd.ShowDialog() == DialogResult.OK) {
        CryptoCurrency? newCurrency = formCurrencyAdd.NewCurrency;
        if (newCurrency != null) {
          wallet.AddCurrency(newCurrency);
          MessageBox.Show(newCurrency.Name);
        }

      }
    }
        private void btnUpdateCurrency_Click(object sender, EventArgs e) {
            lblBalance.Text = ((CryptoCurrency)cbCurrencies.SelectedItem).Ispis;
        }
    }
}